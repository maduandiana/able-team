import React from "react"
import { Link } from "react-router-dom"

function App() {
  return (
    <div>
      <Link to="/">Главная</Link>
      <Link to="/weather">Погода</Link>
      <Link to="/calculator">Калькулятор</Link>
      <Link to="/movie">Фильмы</Link>
      <Link to="/todo">Заметки</Link>
    </div>
  )
}

export default App
