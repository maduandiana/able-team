import { createBrowserRouter } from "react-router-dom";

// import App from './App';
import Main from "./components/Main"
import TodoApp from "./todo_list/todoApp"
import WeatherApp from "./weather/weather"
import Calculator from "./calculator/calculator"
import MoviePage from "./MovieApp/Pages/MoviePage"
import MovieApp from "./MovieApp/Pages/MovieApp"

const router = createBrowserRouter([
    // {
    //     path: '/',
    //     element: <App />
    // },
    {
        path: '/',
        element: <Main />
    },
    {
        path: '/todo',
        element: <TodoApp />
    },
    {
        path: '/weather',
        element: <WeatherApp />
    },
    {
        path: '/calculator',
        element: <Calculator />
    },
    {
        path: '/movie',
        element: <MovieApp />
    },
    {
        path: '/movie/:id',
        element: <MoviePage />
    },
])

export default router;